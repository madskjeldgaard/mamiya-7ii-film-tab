/*

MAMIYA 7II LIGHT SHIELD CURTAIN LEVER

*/

$fn=100;
width=16.8;
cube_depth=7.0;
thickness=2.6;
soften_amount=0.7;
// The cubey part
difference(){
	difference(){
		/************
		  MAIN SHAPE
		 ***********/
		union(){
			minkowski()
			{
				translate([0,0,soften_amount])cube([width-soften_amount, cube_depth-soften_amount, thickness-(soften_amount*2)],0);
				sphere(r=soften_amount);
			}

			// The circular part
			subtract_from_circle_thick=0.2;
			translate([-0.25,0,0]) minkowski()
			{

				difference(){
					translate([ width/2, 5, soften_amount ]) cylinder( h=thickness-subtract_from_circle_thick-(soften_amount*2), d=width+1, /* An approximation based on the full "outer" ring which is 25.5mm */center=false);
					translate([-3,0,-1]) cube([width+5, width+1, thickness+2],false);
#translate([0,-19,0]) cube([width+5, width, thickness+2],false);
				}

				sphere(r=soften_amount);
			}
		}

		/************
		  HOLES
		 ***********/
		hole_pad=0.001;
		hole_depth=2.5+soften_amount+hole_pad;
		hole_dia=1.25;

		/* TODO: Check exact position*/
		offset_from_bottom=0.25;
		rotate([0,-90,0])
			translate([thickness/2,cube_depth-hole_dia-offset_from_bottom,-hole_depth+(hole_pad+soften_amount)])
			cylinder(hole_depth,d=hole_dia,center=false);

		rotate([0,-90,0])
			translate([thickness/2,cube_depth-hole_dia-offset_from_bottom,-width-(hole_pad+soften_amount)])
			cylinder(hole_depth,d=hole_dia,center=false);

		/************
		  REMOVE CENTER
		 ***********/

		// Subtract from bottom
		sub_width=5.4 + 2; // Pad a bit
		sub_height=7.9 + (thickness / 2); // Pad a bit
		translate([(width-soften_amount)/2, sub_height/2+(thickness/2)-1, thickness/2]) cube([sub_width, sub_height, thickness+1],center=true);
	}
/* rotate([0, 0, 60]) translate([width-14, -(3*cube_depth)+4.5, 0])cube([10, 5, 5]); */
/* #translate([-width+10, 0, 0])rotate([0, 0, 60]) translate([width-14, -(3*cube_depth)+4.5, 0])cube([10, 5, 5]); */
}
